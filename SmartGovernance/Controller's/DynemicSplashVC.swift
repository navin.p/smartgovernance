

import UIKit

class DynemicSplashVC: UIViewController {
    @IBOutlet weak var imgView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        if(nsud.value(forKey: "SmartGovernance_LoginStatus") != nil){
            let dictLoginData = nsud.value(forKey: "Smart_EmployeeDetail")as! NSDictionary
            let urlImage = "\(BaseURLSplash)\(dictLoginData.value(forKey: "SplashScreen")!)"
            if(urlImage.count == 0){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC")as! DashBoardVC
                self.navigationController?.pushViewController(testController, animated: false)
            }else{
                imgView.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                    print(url ?? 0)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) { // Change `2.0` to the desired number of seconds.
                        let testController = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC")as! DashBoardVC
                        self.navigationController?.pushViewController(testController, animated: false)
                    }
                }, usingActivityIndicatorStyle: .gray)
            }
        
        }else{
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC")as! DashBoardVC
            self.navigationController?.pushViewController(testController, animated: false)
        }
  
    }
    
    
    
}
