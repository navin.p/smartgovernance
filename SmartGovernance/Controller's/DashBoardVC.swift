//
//  DashBoardVC.swift
//  SmartGovernance
//
//  Created by Navin Patidar on 3/4/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

//MARK:
//MARK: ---------------Protocol
protocol DrawerScreenDelegate : class{
    func refreshDrawerScreen(strType : String ,tag : Int)
}

extension DashBoardVC: DrawerScreenDelegate {
    func refreshDrawerScreen(strType: String, tag: Int) {
        if(strType == "Home"){
            
        }else if(strType == "Profile"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "MyProfileVC")as! MyProfileVC
            self.navigationController?.pushViewController(testController, animated: true)
        }
        else if(strType == "My Task"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "MyTaskVC")as! MyTaskVC
            self.navigationController?.pushViewController(testController, animated: true)
        }
        else if(strType == "Add Category"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "AddCategoryVC")as! AddCategoryVC
            self.navigationController?.pushViewController(testController, animated: true)
        }else if(strType == "Add Employee"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "AddEmployeeVC")as! AddEmployeeVC
            self.navigationController?.pushViewController(testController, animated: true)
        }else if(strType == "Add Subcategory"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "AddSubCategoryVC")as! AddSubCategoryVC
            self.navigationController?.pushViewController(testController, animated: true)
        }
        else if(strType == "Developer Info" || strType == "Disclaimer"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "WebViewVC")as! WebViewVC
            testController.strViewComeFrom = strType
            if (strType == "Disclaimer"){
                testController.webURL = URL_Disclaimer
            }else if (strType == "Developer Info"){
                testController.webURL = URL_DevelopersInfo
            }
            self.navigationController?.pushViewController(testController, animated: true)
        }
        
        else if(strType == "Logout"){
            let alert = UIAlertController(title: alertMessage, message: alertLogout, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "No", style: .default, handler: { action in
                print("Yay! You brought your towel!")
            }))
            alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { action in
                UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
                self.navigationController?.pushViewController(testController, animated: true)
            }))
           
            self.present(alert, animated: true)
        }else{
          
        }
    }
}

// MARK: - --------------DashBoardVC Class
// MARK: -

class DashBoardVC: UIViewController {
    
    // MARK: - -------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var viewPluse: CardView!
    @IBOutlet weak var viewError: UIView!
    
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var lblShowDate: UILabel!
    
    @IBOutlet weak var btnTotalTask: UIButton!
    @IBOutlet weak var btnOverDue: UIButton!
    @IBOutlet weak var btnDue: UIButton!
    @IBOutlet weak var btnActionTaken: UIButton!
    @IBOutlet weak var btnCompleted: UIButton!
    
    
    @IBOutlet weak var txtStartDate: ACFloatingTextfield!
    @IBOutlet weak var txtDueDate: ACFloatingTextfield!
    @IBOutlet var viewDateFilter: UIView!
    @IBOutlet weak var btbOK: UIButton!
    
    @IBOutlet var viewPicker: UIView!
    @IBOutlet weak var picker: UIDatePicker!
    @IBOutlet weak var btnDone: UIButton!
    
    @IBOutlet weak var lblPersent: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lbl4: UILabel!
    var slices = NSMutableArray()
    var sliceColors = NSMutableArray()

    @IBOutlet weak var pieChartLeft: XYPieChart!
    
    // MARK: - --------------LifeCycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        viewPluse.backgroundColor = hexStringToUIColor(hex: primaryColor)
        btnDone.backgroundColor = hexStringToUIColor(hex: primaryColor)
        btbOK.backgroundColor = hexStringToUIColor(hex: primaryColor)
        lblShowDate.text = "< Today's >"
        viewError.isHidden = true
        print(nsud.value(forKey: "Smart_EmployeeDetail") as! NSDictionary)
      
        if (nsud.value(forKey: "Smart_EmployeeDetail") != nil){
            dict_Login_Data = NSMutableDictionary()
            dict_Login_Data = (nsud.value(forKey: "Smart_EmployeeDetail")as! NSDictionary).mutableCopy()as! NSMutableDictionary
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            let result = formatter.string(from: date)
            let toDate = result
            self.lblShowDate.text = "< Today's >"
            self.callGetTaskManagementDashboardSummaryAPI(sender: UIButton(), startDate: toDate, dueDate: toDate)
        }
    }
       
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        lblPersent.layer.cornerRadius = 25.0
        if(ReloadDashBoardCheck == 1){
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            let result = formatter.string(from: date)
            let toDate = result
            self.lblShowDate.text = "< Today's >"
            self.callGetTaskManagementDashboardSummaryAPI(sender: UIButton(), startDate: toDate, dueDate: toDate)
            ReloadDashBoardCheck = 0
        }
    }
    
    override func viewWillLayoutSubviews() {
        scroll.contentSize = CGSize(width: self.view.frame.width, height: 600)
        
    }
    
    
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actionOnMenu(_ sender: UIButton) {
        self.view.endEditing(true)
        let vc: DrawerVC = self.storyboard!.instantiateViewController(withIdentifier: "DrawerVC") as! DrawerVC
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.handleDrawerView = self
        self.present(vc, animated: false, completion: {})
    }
    
    @IBAction func actionOnMyTask(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "MyTaskVC")as! MyTaskVC
      
        self.navigationController?.pushViewController(testController, animated: true)
    }
    
    @IBAction func actionOnFilter(_ sender: UIButton) {
        self.view.endEditing(true)
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select", message: "Option to Filter", preferredStyle: .actionSheet)
        let CancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(CancelActionButton)
        let TodaysActionButton = UIAlertAction(title: "Today's", style: .default) { _ in
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            let result = formatter.string(from: date)
            let toDate = result
            self.lblShowDate.text = "< Today's >"
            self.callGetTaskManagementDashboardSummaryAPI(sender: UIButton(), startDate: toDate, dueDate: toDate)
        }
        actionSheetControllerIOS8.addAction(TodaysActionButton)
        
        let LastWeekActionButton = UIAlertAction(title: "Last Week", style: .default)
        { _ in
            self.lblShowDate.text = "< Last Week >"
            let aryDate = getPastDates(days: 7)
            let dict = (aryDate.lastObject)as! NSDictionary
            let fromDate = "\(dict.value(forKey: "month")!)/\(dict.value(forKey: "day")!)/\(dict.value(forKey: "year")!)"
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            let result = formatter.string(from: date)
            let toDate = result
            self.callGetTaskManagementDashboardSummaryAPI(sender: UIButton(), startDate: fromDate, dueDate: toDate)
            
        }
        actionSheetControllerIOS8.addAction(LastWeekActionButton)
        
        let Last15daysActionButton = UIAlertAction(title: "Last 15 days", style: .default)
        { _ in
            self.lblShowDate.text = "< Last 15 days >"
            
            let aryDate = getPastDates(days: 15)
            let dict = (aryDate.lastObject)as! NSDictionary
            let fromDate = "\(dict.value(forKey: "month")!)/\(dict.value(forKey: "day")!)/\(dict.value(forKey: "year")!)"
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            let result = formatter.string(from: date)
            let toDate = result
            self.callGetTaskManagementDashboardSummaryAPI(sender: UIButton(), startDate: fromDate, dueDate: toDate)
        }
        actionSheetControllerIOS8.addAction(Last15daysActionButton)
        
        let LastMonthActionButton = UIAlertAction(title: "Last Month", style: .default)
        { _ in
            self.lblShowDate.text = "< Last Month >"
            let aryDate = getPastDates(days: 31)
            let dict = (aryDate.lastObject)as! NSDictionary
            let fromDate = "\(dict.value(forKey: "month")!)/\(dict.value(forKey: "day")!)/\(dict.value(forKey: "year")!)"
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            let result = formatter.string(from: date)
            let toDate = result
            self.callGetTaskManagementDashboardSummaryAPI(sender: UIButton(), startDate: fromDate, dueDate: toDate)
        }
        actionSheetControllerIOS8.addAction(LastMonthActionButton)
        
        
        let NextMonthActionButton = UIAlertAction(title: "Upcoming Month", style: .default)
        { _ in
            self.lblShowDate.text = "< Upcoming Month >"
            let aryDate = getNextDates(days: 31)
            let dict = (aryDate.lastObject)as! NSDictionary
            let toDate = "\(dict.value(forKey: "month")!)/\(dict.value(forKey: "day")!)/\(dict.value(forKey: "year")!)"
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            let result = formatter.string(from: date)
            let fromDate = result
            self.callGetTaskManagementDashboardSummaryAPI(sender: UIButton(), startDate: fromDate, dueDate: toDate)
        }
        actionSheetControllerIOS8.addAction(NextMonthActionButton)
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    @IBAction func actionRemove(_ sender: Any) {
        self.viewDateFilter.removeFromSuperview()
    }
    
    @IBAction func actionOk(_ sender: Any) {
        if(txtStartDate.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertStartDate, viewcontrol: self)
        }else if (txtDueDate.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDueDate, viewcontrol: self)
            
        }else{
            callGetTaskManagementDashboardSummaryAPI(sender: UIButton(), startDate: txtStartDate.text!, dueDate: txtDueDate.text!)
            lblShowDate.text = txtStartDate.text! + " - " + txtDueDate.text!
            self.viewDateFilter.removeFromSuperview()
            txtDueDate.text = ""
            txtStartDate.text = ""
        }
    }
    
    @IBAction func actionOnStart_DueDate(_ sender: UIButton) {
        self.viewPicker.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
       // picker.maximumDate = Date()
        picker.tag = sender.tag
        self.view.addSubview(self.viewPicker)
    }
    @IBAction func actionOnSelectDate(_ sender: UIButton) {
        self.viewDateFilter.frame =  CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.view.addSubview(self.viewDateFilter)
    }
    @IBAction func actionONDone(_ sender: UIButton) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let somedateString = dateFormatter.string(from: picker.date)
        if picker.tag == 1 {
            txtStartDate.text = somedateString
        }else{
            txtDueDate.text = somedateString
        }
        
        if(txtStartDate.text != "" && txtDueDate.text != ""){
            let strDueDate = dateFormatter.date(from:txtDueDate.text!)!
            let strStartDate = dateFormatter.date(from:txtStartDate.text!)!
            if(strStartDate >= strDueDate){
               txtDueDate.text = ""
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDateCheck, viewcontrol: self)
            }
        }
    
        self.viewPicker.removeFromSuperview()
    }
    
    @IBAction func actionAddTask(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "AddTaskVC")as! AddTaskVC
       ReloadDashBoardCheck = 1
        self.navigationController?.pushViewController(testController, animated: true)
    }
    // MARK: - --------------Show Error / Network
    // MARK: -
    
    func showErrorWithImage( strTitle : String , imgError : UIImage)   {
        removeErrorView()
        addErrorImageOnView(strMessage: strTitle, img: imgError, controller: self, view: viewHeader)
        btnRetry.addTarget(self, action: #selector(sayAction(_:)), for: .touchUpInside)
    }
    @objc private func sayAction(_ sender: UIButton!) {
        removeErrorView()
        self.viewDidLoad()
    }
    
    // MARK: - ---------------API Calling
    // MARK: -
    func callGetTaskManagementDashboardSummaryAPI(sender : UIButton , startDate : String , dueDate : String) {
        if !isInternetAvailable() {
            self.showErrorWithImage(strTitle: alertInternet, imgError: strInternetImage)
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetTaskManagementDashboardSummary xmlns='\(BaseURL0)'><StartDate>\(startDate)</StartDate><DueDate>\(dueDate)</DueDate><AssignByEmpId>\(dict_Login_Data.value(forKey: "EmpId")!)</AssignByEmpId></GetTaskManagementDashboardSummary></soap12:Body></soap12:Envelope>"
            print(soapMessage)
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetTaskManagementDashboardSummaryResult", responcetype: "GetTaskManagementDashboardSummaryResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        let aryTemp = dictTemp.value(forKey: "DashboardSummary")as! NSArray
                        self.setPieChartData(aryData: aryTemp)
                    }else{
                            self.viewError.isHidden = false
                    }
                }else{
                        self.viewError.isHidden = false
                }
            }
        }
    }
    func setPieChartData(aryData : NSArray)  {
        let dictdata = aryData.object(at: 0)as! NSDictionary
              let strOverdue = "\(dictdata.value(forKey: "Overdue")!)"
                let strDue = "\(dictdata.value(forKey: "Due")!)"
                let strActionTaken = "\(dictdata.value(forKey: "ActionTaken")!)"
                let strCompleted = "\(dictdata.value(forKey: "Completed")!)"
                let strTotal = "\(dictdata.value(forKey: "Total")!)"

       btnOverDue.setTitle(strOverdue, for: .normal)
        btnDue.setTitle(strDue, for: .normal)
        btnActionTaken.setTitle(strActionTaken, for: .normal)
        btnCompleted.setTitle(strCompleted, for: .normal)
        btnTotalTask.setTitle(strTotal, for: .normal)
        if(strTotal == "0" || strTotal == "" ){
            viewError.isHidden = false
        }else{
            viewError.isHidden = true
            lbl2.text = "Due"
            lbl3.text = "Action Taken"
            lbl4.text = "Completed"

            self.sliceColors = NSMutableArray()
            self.sliceColors.add(hexStringToUIColor(hex: primaryOrangeColor))
            self.sliceColors.add(hexStringToUIColor(hex: primaryBlueColor))
            self.sliceColors.add(hexStringToUIColor(hex: primaryGreenColor))
            self.slices = NSMutableArray()
            self.slices.add(Int(strDue)!)
         self.slices.add(Int(strActionTaken)!)
          self.slices.add(Int(strCompleted)!)
            pieChartLeft.dataSource = self
            pieChartLeft.startPieAngle = .pi/2
            pieChartLeft.animationSpeed = 1.0
            pieChartLeft.labelFont =  UIFont.boldSystemFont(ofSize: 20.0)

            pieChartLeft.labelRadius = 50
            pieChartLeft.showPercentage = true
            pieChartLeft.setPieBackgroundColor(UIColor(white: 0.95, alpha: 1))
            pieChartLeft.pieCenter = pieChartLeft.center
            pieChartLeft.isUserInteractionEnabled = false
            pieChartLeft.labelShadowColor = UIColor.black
            pieChartLeft.labelColor = UIColor.white
            pieChartLeft.reloadData()
            lblPersent.layer.cornerRadius = 25.0
            lblPersent.clipsToBounds = true
            lblPersent.center = pieChartLeft.center
            viewError.isHidden = true
        }
    }
}

extension DashBoardVC : XYPieChartDelegate, XYPieChartDataSource{
    func numberOfSlices(in pieChart: XYPieChart!) -> UInt {
        return UInt(slices.count)
    }
    func pieChart(_ pieChart: XYPieChart!, valueForSliceAt index: UInt) -> CGFloat {
        return slices.object(at: Int(index)) as! CGFloat

    }
    func pieChart(_ pieChart: XYPieChart!, colorForSliceAt index: UInt) -> UIColor! {
        return (sliceColors.object(at: Int(index)) as! UIColor)
    }
    func pieChart(_ pieChart: XYPieChart!, willSelectSliceAt index: UInt) {
        
    }
}
