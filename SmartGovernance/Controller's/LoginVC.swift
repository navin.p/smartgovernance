//
//  LoginVC.swift
//  SmartGovernance
//
//  Created by Navin Patidar on 3/6/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
    
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var viewLogIn: CardView!

    @IBOutlet weak var txtUserName: ACFloatingTextfield!
    @IBOutlet weak var txtPassword: ACFloatingTextfield!
    @IBOutlet weak var imgViewSplash: UIImageView!
    @IBOutlet weak var tv: UITableView!

    // MARK: - --------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
     
         viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
         viewLogIn.backgroundColor = hexStringToUIColor(hex: primaryColor)
//        txtUserName.text = "idaceoindore"
//        txtPassword.text = "ida!ceovivek"

        let blurEffect = UIBlurEffect(style: .extraLight)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame
        
        self.imgViewSplash.insertSubview(blurEffectView, at: 0)
    }
    
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actionOnLogin(_ sender: UIButton) {
        if(validation()){
       
            callLoginAPI(sender: sender)
        }
        self.view.endEditing(true)
    }
    @IBAction func actionOnSignup(_ sender: UIButton) {
        self.view.endEditing(true)
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "RegistrationVC")as! RegistrationVC
        self.navigationController?.pushViewController(testController, animated: true)
    }
    @IBAction func actionOnForgotPassword(_ sender: UIButton) {
        self.view.endEditing(true)
       self.viewForgotAlert()
    }
   
    // MARK: - ---------------Validation
    // MARK: -
    func validation() -> Bool {
        if(txtUserName.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertUserName, viewcontrol: self)
            return false
        }else if (txtPassword.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertPassword, viewcontrol: self)
            return false
        }
        return true
    }
    func viewForgotAlert()  {
        let alertController = UIAlertController(title: "Forgot Password", message: "Enter the emailID associated with your  account in the text box and then click the Send  button.", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Send", style: .default, handler: { alert -> Void in
            let textField = alertController.textFields![0] as UITextField
            if !(validateEmail(email: textField.text!)){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertEmailValid, viewcontrol: self)
                self.viewForgotAlert()
            }else{
                self.callForgotPasswordAPI(sender: textField.text!)
            }
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "Email Address"
            textField.borderStyle = UITextField.BorderStyle.roundedRect
            textField.delegate = self
            textField.tag = 99
        })
        self.present(alertController, animated: true, completion: nil)
    }
    // MARK: - ---------------API Calling
    // MARK: -
    func callLoginAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
        
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetLogin xmlns='\(BaseURL0)'><username>\(txtUserName.text!)</username><password>\(txtPassword.text!)</password></GetLogin></soap12:Body></soap12:Envelope>"
          
            dotLoader(sender: sender , controller : self, viewContain: self.viewLogIn, onView: "Button")
          
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetLoginResult", responcetype: "GetLoginResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        let aryTemp = dictTemp.value(forKey: "EmployeeDetail")as! NSArray
                        if(aryTemp.count != 0){
                            let dict = (aryTemp.object(at: 0)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                            nsud.set(removeNullFromDict(dict: dict), forKey: "Smart_EmployeeDetail")
                            nsud.set("True", forKey: "SmartGovernance_LoginStatus")
                            nsud.synchronize()
                            let testController = mainStoryboard.instantiateViewController(withIdentifier: "DynemicSplashVC")as! DynemicSplashVC
                            self.navigationController?.pushViewController(testController, animated: true)
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dictTemp.value(forKey: "Msg")!)", viewcontrol: self)
                    }
                }else{
                     showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    func callForgotPasswordAPI(sender : String) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><ForgetPassword xmlns='\(BaseURL0)'><emailid>\(sender)</emailid></ForgetPassword></soap12:Body></soap12:Envelope>"
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "ForgetPasswordResult", responcetype: "ForgetPasswordResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    let msg = "\(dictTemp.value(forKey: "Msg")!)"
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: msg, viewcontrol: self)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: msg, viewcontrol: self)
                    }
                }else{
                   showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -

extension LoginVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 50)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
}
