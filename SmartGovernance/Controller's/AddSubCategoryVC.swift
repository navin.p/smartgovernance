
//
//  AddSubCategoryVC.swift
//  SmartGovernance
//
//  Created by Navin Patidar on 3/7/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class AddSubCategoryVC: UIViewController {
    
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var txtSubCategoryName: ACFloatingTextfield!
    @IBOutlet weak var txtCategoryName: ACFloatingTextfield!
    @IBOutlet weak var txtEmployeeName: ACFloatingTextfield!
      var aryCategory = NSMutableArray()
      var aryEmployee = NSMutableArray()
     var arySelectedEmployee = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        buttonRounded(sender: btnSave)
        self.callCategoryListAPI()
        self.callEmployeeListAPI()
        // Do any additional setup after loading the view.
    }
  
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionOnSave(_ sender: UIButton) {
        self.view.endEditing(true)
        if(validation()){
            callAddSubCategoryAPI(sender: sender)
        }
    }
    @IBAction func actionOnCategory(_ sender: UIButton) {
        sender.tag = 1
        self.view.endEditing(true)
        if(self.aryCategory.count == 0){
            FTIndicator.showToastMessage(alertCategoryList)
        }else{
            gotoPopViewWithArray(sender: sender, aryData: self.aryCategory, strTitle: "\(txtSubCategoryName.placeholder!)")
        }
    }
    @IBAction func actionOnEmployee(_ sender: UIButton) {
        sender.tag = 2
        self.view.endEditing(true)
        if(self.aryEmployee.count == 0){
            FTIndicator.showToastMessage(alertEmployeeList)
        }else{
            gotoPopViewWithArray(sender: sender, aryData: self.aryEmployee, strTitle: "\(txtEmployeeName.placeholder!)")
        }
    }
    // MARK: - ---------------validation
    // MARK: -
    func validation() -> Bool {

        if(txtCategoryName.tag == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSelectCategoryName, viewcontrol: self)
            return false
        }else if (txtEmployeeName.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSelectemployee, viewcontrol: self)
            return false
            
        }else if (txtSubCategoryName.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSubCategoryName, viewcontrol: self)
            return false
        }
        return true
    }
    
    // MARK: - ---------------PopUPView
    // MARK: -
    func gotoPopViewWithArray(sender: UIButton , aryData : NSMutableArray , strTitle: String)  {
        let vc: PopUpView = self.storyboard!.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.strTitle = strTitle
        vc.delegate = self
        vc.strTag = sender.tag
        if aryData.count != 0{
            vc.aryTBL = aryData
            vc.arySelected = arySelectedEmployee
            self.present(vc, animated: false, completion: {})
        }
    }
    
    // MARK: - ---------------API Calling
    // MARK: -
    func callAddSubCategoryAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
            
            let aryAddEmp = NSMutableArray()
            for item in arySelectedEmployee{
                let dict = NSMutableDictionary()
                dict.setValue("0", forKey: "TMEmpCatId")
                dict.setValue("\((item as AnyObject).value(forKey: "TMEmpId")!)", forKey: "TMEmpId")
                aryAddEmp.add(dict)
            }
            var json = Data()
            var jsonString = NSString()
            if JSONSerialization.isValidJSONObject(aryAddEmp) {
                // Serialize the dictionary
                json = try! JSONSerialization.data(withJSONObject: aryAddEmp, options: .prettyPrinted)
                jsonString = String(data: json, encoding: .utf8)! as NSString
                print("UpdateLeadinfo JSON: \(jsonString)")
            }
            if(aryAddEmp.count == 0){
                jsonString = ""
            }
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AddTaskSubCategoryWithEmployee xmlns='\(BaseURL0)'><TaskSubCategoryId>0</TaskSubCategoryId><TaskCategoryId>\(txtCategoryName.tag)</TaskCategoryId><TaskSubCategoryName>\(txtSubCategoryName.text!)</TaskSubCategoryName><IsActive>true</IsActive><AddEmpDc>\(jsonString)</AddEmpDc></AddTaskSubCategoryWithEmployee></soap12:Body></soap12:Envelope>"
            
//            TaskSubCategoryId...: 0
//            TaskCategoryId...: 16
//            TaskSubCategoryName...: test by ios
//            IsActive...: true
//            AddEmpDc...: [{"TMEmpCatId":0,"TMEmpId":45},{"TMEmpCatId":0,"TMEmpId":35},{"TMEmpCatId":0,"TMEmpId":35},{"TMEmpCatId":0,"TMEmpId":36},{"TMEmpCatId":0,"TMEmpId":37},{"TMEmpCatId":0,"TMEmpId":45},{"TMEmpCatId":0,"TMEmpId":35},{"TMEmpCatId":0,"TMEmpId":36}]
//            AddTaskSubCategoryWithEmployee (wEB SERVICE FOR ADD SUB CATEGORY)
            
            dotLoader(sender: sender , controller : self, viewContain: self.view, onView: "Button")
            
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "AddTaskSubCategoryWithEmployeeResult", responcetype: "AddTaskSubCategoryWithEmployeeResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    FTIndicator.showNotification(withTitle: alertInfo, message: "Sub Category Added Successfully!")
                    self.navigationController?.popViewController(animated: true)
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
         
            }
        }
    }
    
    // MARK: - ---------------API Calling
    // MARK: -
    func callCategoryListAPI() {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }else{
            //
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetCategory xmlns='\(BaseURL0)'><EmpId>\(dict_Login_Data.value(forKey: "EmpId")!)</EmpId></GetCategory></soap12:Body></soap12:Envelope>"
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetCategoryResult", responcetype: "GetCategoryResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        self.aryCategory = NSMutableArray()
                        self.aryCategory = (dictTemp.value(forKey: "Category")as! NSArray).mutableCopy()as! NSMutableArray
                    }else{
                        //self.showErrorWithImage(strTitle: alertDataNotFound, imgError: strDataNotFoundImage)
                    }
                }else{
                    // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    func callEmployeeListAPI() {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }else{
            //
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetTaskEmployeeMaster xmlns='\(BaseURL0)'><AssignByEmpId>\(dict_Login_Data.value(forKey: "EmpId")!)</AssignByEmpId></GetTaskEmployeeMaster></soap12:Body></soap12:Envelope>"
           // dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetTaskEmployeeMasterResult", responcetype: "GetTaskEmployeeMasterResponse") { (responce, status) in
             //   remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        self.aryEmployee = NSMutableArray()
                        self.aryEmployee = (dictTemp.value(forKey: "TaskEmployee")as! NSArray).mutableCopy()as! NSMutableArray
                    }else{
                        //self.showErrorWithImage(strTitle: alertDataNotFound, imgError: strDataNotFoundImage)
                    }
                }else{
                    // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension AddSubCategoryVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtSubCategoryName  {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 45)
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
}

//MARK:-
//MARK:- ---------PopUpViewDelegate Methods----------

extension AddSubCategoryVC: PopUpViewDelegate {
    func getDataFromPopUpView(dictData: NSDictionary, tag: Int) {
        self.view.endEditing(true)
        if tag == 1 {
            txtCategoryName.text = (dictData.value(forKey: "TaskCategoryName")as! String)
            txtCategoryName.tag = Int("\(dictData.value(forKey: "TaskCategoryId")!)")!
        }else if (tag == 2){
            if(dictData.count != 0){
                let ary = dictData.value(forKey: "selected")as! NSArray
                var strName = ""
                
                for  item in ary{
                    strName = strName + "\((item as AnyObject).value(forKey: "TMEmpName")!),"
                }
                
                txtEmployeeName.text = String(strName.dropLast())
                
                arySelectedEmployee = NSMutableArray()
                arySelectedEmployee = ary.mutableCopy()as! NSMutableArray
            }
        }
    }
}

