//
//  AddCategoryVC.swift
//  SmartGovernance
//
//  Created by Navin Patidar on 3/6/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class AddCategoryVC: UIViewController {

    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var txtCategoryName: ACFloatingTextfield!
    override func viewDidLoad() {
        super.viewDidLoad()
         viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
         buttonRounded(sender: btnSave)
    }
    
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.view.endEditing(true)
     self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionOnSave(_ sender: UIButton) {
        self.view.endEditing(true)
        if(txtCategoryName.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCategoryName, viewcontrol: self)
        }else{
            callAddCategoryAPI(sender: sender)
        }
    }
    // MARK: - ---------------API Calling
    // MARK: -
    func callAddCategoryAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AddTaskCategory xmlns='\(BaseURL0)'><TaskCategoryId>0</TaskCategoryId><AssignByEmpId>\(dict_Login_Data.value(forKey: "EmpId")!)</AssignByEmpId><TaskCategoryName>\(txtCategoryName.text!)</TaskCategoryName><IsActive>true</IsActive></AddTaskCategory></soap12:Body></soap12:Envelope>"
            dotLoader(sender: sender , controller : self, viewContain: self.view, onView: "Button")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "AddTaskCategoryResult", responcetype: "AddTaskCategoryResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        FTIndicator.showNotification(withTitle: alertInfo, message: "Category Added Successfully!")
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension AddCategoryVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtCategoryName  {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 45)
        }
    
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
}
