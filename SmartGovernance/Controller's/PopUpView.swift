//

//SUB Category Screen
//TAG = 1, 2  Category , Employee

//Add TaskScreen
//TAG = 3, 4  Category , SubCategory

//AddSubTaskScreen
//TAG = 5, 6, 7  Priority , Status, Assign to

import UIKit
//MARK:
//MARK: ---------------Protocol-----------------

protocol PopUpViewDelegate : class{
    func getDataFromPopUpView(dictData : NSDictionary ,tag : Int)
}

class PopUpView: UIViewController {
    
    @IBOutlet weak var btnTitle: UIButton!
    @IBOutlet weak var tvForList: UITableView!
    @IBOutlet weak var height_ForTBLVIEW: NSLayoutConstraint!
    @IBOutlet weak var viewFortv: CardView!
    @IBOutlet weak var btnOK: UIButton!

    var strTitle = String()
    var aryTBL = NSMutableArray()
    var arySelected = NSMutableArray()

    @IBOutlet weak var btnOkHeight: NSLayoutConstraint!
    var strTag = Int()

    weak var handleAddTaskScreen: AddTaskScreenDelegate?
    weak var handleAddSubTaskScreen : AddSubTaskScreenDelegate?
    open weak var delegate:PopUpViewDelegate?


    override func viewDidLoad() {
        super.viewDidLoad()
        tvForList.estimatedRowHeight = 50.0
        btnTitle.setTitle(strTitle, for: .normal)
        tvForList.tableFooterView = UIView()
        tvForList.dataSource = self
        tvForList.delegate = self
        self.view.backgroundColor = UIColor.clear
        print(strTag)
        btnTitle.setTitleColor(hexStringToUIColor(hex: primaryColor), for: .normal)
   
    }
    
    override func viewWillLayoutSubviews() {
     
        
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(strTag == 2 || strTag == 7){
            btnOkHeight.constant = 40.0
        }else{
            btnOkHeight.constant = 0.0
        }
        height_ForTBLVIEW.constant  = 0.0
        height_ForTBLVIEW.constant = CGFloat((aryTBL.count * 40 ) + 50 )
        if  height_ForTBLVIEW.constant > self.view.frame.size.height - 150  {
            height_ForTBLVIEW.constant = self.view.frame.size.height - 150
        }
        self.viewFortv.frame = CGRect(x: 15, y: Int(self.view.frame.height) / 2 - Int(self.height_ForTBLVIEW.constant) / 2 , width: Int(self.view.frame.width) - 30 , height: Int(self.height_ForTBLVIEW.constant))
        self.viewFortv.center = self.view.center
        
      
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
      self.dismiss(animated: false) { }
        
    }
    @IBAction func actionOnOK(_ sender: UIButton) {
        if(strTag == 2) {
            let dict = NSMutableDictionary()
            dict.setValue(arySelected, forKey: "selected")
            delegate?.getDataFromPopUpView(dictData: dict as NSDictionary, tag: strTag)
        } else if(strTag == 7) {
            let dict = NSMutableDictionary()
            dict.setValue(arySelected, forKey: "selected")
            handleAddSubTaskScreen?.AddSubTaskScreen(dictData: dict, tag: strTag)
        }
        self.dismiss(animated: false) {}

    }
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension PopUpView : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryTBL.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvForList.dequeueReusableCell(withIdentifier: "popupCell", for: indexPath as IndexPath) as! popupCell
        let dict = aryTBL.object(at: indexPath.row)as! NSDictionary
        if(strTag == 1 || strTag == 3){
            cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "TaskCategoryName")as! String)"
        }else if(strTag == 2 || strTag == 7){
            cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "TMEmpName")as! String)"
            if(arySelected.contains(dict)){
                cell.accessoryType = .checkmark
            }else{
                cell.accessoryType = .none
            }
        }else if (strTag == 5 || strTag == 6){
            cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Name")as! String)"
        }else if (strTag == 4 ){
            cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "TaskSubCategoryName")as! String)"
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = aryTBL.object(at: indexPath.row)as! NSDictionary
        
        if(strTag == 1) {
            delegate?.getDataFromPopUpView(dictData: dict, tag: strTag)
           self.dismiss(animated: false) {}

        }else if(strTag == 2 || strTag == 7) {
            if(arySelected.contains(dict)){
                 arySelected.remove(dict)
            }else{
                arySelected.add(dict)
            }
            tvForList.reloadData()
        }else if(strTag == 3 || strTag == 4) {
            handleAddTaskScreen?.AddTaskScreenScreen(dictData: dict, tag: strTag)
        self.dismiss(animated: false) {}
        }else if(strTag == 5 || strTag == 6 ) {
            handleAddSubTaskScreen?.AddSubTaskScreen(dictData: dict, tag: strTag)
            self.dismiss(animated: false) {}
        }
    }
}

class popupCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var popupCell_lbl_Title: UILabel!
}



