//
//  AddTaskVC.swift
//  SmartGovernance
//
//  Created by Navin Patidar on 3/7/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class AddTaskVC: UIViewController {
    
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var viewPluseButton: CardView!
    @IBOutlet weak var viewContain: UIView!

    @IBOutlet weak var txtSelectCategory: ACFloatingTextfield!
    @IBOutlet weak var txtSelectSubCategory: ACFloatingTextfield!

      @IBOutlet weak var txtTaskName: ACFloatingTextfield!
    
    @IBOutlet weak var txtDescription: KMPlaceholderTextView!
    
    @IBOutlet weak var tvSubTaskList: UITableView!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var heightTV: NSLayoutConstraint!

    var aryTaskList = NSMutableArray()
    var aryCategory = NSMutableArray()
    var arySubCategory = NSMutableArray()
     var handleDashBoardTaskScreen: DrawerScreenDelegate?
    var strTag = Int()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        buttonRounded(sender: btnSave)
        viewPluseButton.backgroundColor = hexStringToUIColor(hex: primaryColor)
         callCategoryListAPI()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(aryTaskList.count == 0){
            heightTV.constant = 0.0
        }else{
            heightTV.constant = CGFloat((aryTaskList.count * 85))
        }
        viewContain.frame = CGRect(x: 0, y: 0, width: Int(self.view.frame.width), height: Int (heightTV.constant) + 520)
        scroll.contentSize = CGSize(width: Int(self.view.frame.width), height: Int (heightTV.constant) + 520)
        viewContain.removeFromSuperview()
        scroll.addSubview(viewContain)
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        handleDashBoardTaskScreen?.refreshDrawerScreen(strType: "", tag: strTag)

        self.view.endEditing(true)
       self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func actionAddSubTask(_ sender: UIButton) {
        if(txtSelectCategory.tag == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSelectCategoryName, viewcontrol: self)
        }else if (txtSelectSubCategory.tag == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSelectSubCategoryName, viewcontrol: self)
        }else{
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "AddSubTaskVC")as! AddSubTaskVC
            testController.strSubCategoryID = "\(txtSelectSubCategory.tag)"
            testController.handle_AddTaskScreen = self
            self.navigationController?.pushViewController(testController, animated: true)
        }
    }
    
    @IBAction func actionSelectCategory(_ sender: UIButton) {
        sender.tag = 3
        self.view.endEditing(true)
        if(self.aryCategory.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCategoryList, viewcontrol: self)
        }else{
            gotoPopViewWithArray(sender: sender, aryData: self.aryCategory, strTitle: "\(txtSelectCategory.placeholder!)")
        }
    }
    @IBAction func actionSelectSubCategory(_ sender: UIButton) {
        if(txtSelectCategory.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCategoryFirst, viewcontrol: self)
        }else{
            sender.tag = 4
            self.view.endEditing(true)
                if(self.arySubCategory.count == 0){
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSubCategoryList, viewcontrol: self)
                }else{
                    gotoPopViewWithArray(sender: sender, aryData: arySubCategory, strTitle: "\(txtSelectSubCategory.placeholder!)")
                }
            }
   
    }
    
    @IBAction func actionOnSave(_ sender: UIButton) {
        self.view.endEditing(true)
        if(validation()){
            callAddTaskAPI(sender: sender)
        }
    }
    // MARK: - ---------------validation
    // MARK: -
    func validation() -> Bool {
        if(txtSelectCategory.tag == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSelectCategoryName, viewcontrol: self)
            return false
        }else if (txtSelectSubCategory.tag == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSelectSubCategoryName, viewcontrol: self)
            return false
            
        }else if (txtTaskName.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertTaskName, viewcontrol: self)
            return false
        }else if (txtDescription.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertTaskDescription, viewcontrol: self)
            return false
        }
        return true
    }
    // MARK: - ---------------API Calling
    // MARK: -
    func callAddTaskAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
            
            let dictData = NSMutableDictionary()
            dictData.setValue("0", forKey: "TaskManagementId")
            dictData.setValue("\(txtSelectCategory.tag)", forKey: "TaskCategoryId")
            dictData.setValue("\(txtSelectSubCategory.tag)", forKey: "TaskSubCategoryId")
            dictData.setValue("\(dict_Login_Data.value(forKey: "EmpId")!)", forKey: "AssignByEmpId")
            dictData.setValue("\(txtTaskName.text!)", forKey: "TaskName")
            dictData.setValue("\(txtDescription.text!)", forKey: "TaskDesc")
            dictData.setValue("", forKey: "StartDate")
            dictData.setValue("", forKey: "DueDate")
            dictData.setValue("", forKey: "Status")
            dictData.setValue("", forKey: "Priority")
            dictData.setValue(aryTaskList, forKey: "AddSubTask")
            
            
            var json1 = Data()
            var jsonAdd = NSString()
            let jsonSendArray = NSMutableArray()
            jsonSendArray.add(dictData)
            if JSONSerialization.isValidJSONObject(jsonSendArray) {
                // Serialize the dictionary
                json1 = try! JSONSerialization.data(withJSONObject: jsonSendArray, options: .prettyPrinted)
                jsonAdd = String(data: json1, encoding: .utf8)! as NSString
                print("UpdateLeadinfo JSON: \(jsonAdd)")
            }
            if(jsonSendArray.count == 0){
                jsonAdd = ""
            }
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AddTaskManagement xmlns='\(BaseURL0)'><AddTaskDc>\(jsonAdd)</AddTaskDc></AddTaskManagement></soap12:Body></soap12:Envelope>"
            
            dotLoader(sender: sender , controller : self, viewContain: self.view, onView: "Button")
            
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "AddTaskManagementResult", responcetype: "AddTaskManagementResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    FTIndicator.showNotification(withTitle: alertInfo, message: "Task Added Successfully!")
                    self.navigationController?.popViewController(animated: true)
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    func callCategoryListAPI() {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }else{
            //
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetCategory xmlns='\(BaseURL0)'><EmpId>\(dict_Login_Data.value(forKey: "EmpId")!)</EmpId></GetCategory></soap12:Body></soap12:Envelope>"
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetCategoryResult", responcetype: "GetCategoryResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        self.aryCategory = NSMutableArray()
                        self.aryCategory = (dictTemp.value(forKey: "Category")as! NSArray).mutableCopy()as! NSMutableArray
                    }else{
                        //self.showErrorWithImage(strTitle: alertDataNotFound, imgError: strDataNotFoundImage)
                    }
                }else{
                    // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    // MARK: - ---------------PopUPView
    // MARK: -
    func gotoPopViewWithArray(sender: UIButton , aryData : NSMutableArray , strTitle: String)  {
        let vc: PopUpView = self.storyboard!.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = strTitle
        vc.strTag = sender.tag
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handleAddTaskScreen = self
            vc.aryTBL = aryData
            self.present(vc, animated: false, completion: {})
        }
    }
    
}
// MARK: - ---------------UITableViewDelegate
// MARK: -
extension AddTaskVC : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryTaskList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvSubTaskList.dequeueReusableCell(withIdentifier: "SubTaskCell", for: indexPath as IndexPath) as! SubTaskCell
        let dict = aryTaskList.object(at: indexPath.row)as! NSDictionary
        cell.lblTitle.text = "Subtask Name : \(dict.value(forKey: "SubTaskName")!)\n\nDescription : \(dict.value(forKey: "SubTaskDesc")!)"

        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0.4
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 1.0) {
            cell.alpha = 1
            cell.transform = .identity
        }
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
           aryTaskList.removeObject(at: indexPath.row)
            tvSubTaskList.reloadData()
        }
    }
}
// MARK: - ----------------UserDashBoardCell
// MARK: -
class SubTaskCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTitleDetail: UILabel!

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}

// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension AddTaskVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtTaskName  {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 45)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
        
    }
   
}


//MARK:
//MARK: ---------------Protocol-----------------

protocol AddTaskScreenDelegate : class{
    func AddTaskScreenScreen(dictData : NSDictionary ,tag : Int)
}
//MARK:-
//MARK:- ---------RegistrationScreenDelegate Methods----------

extension AddTaskVC: AddTaskScreenDelegate {
    func AddTaskScreenScreen(dictData: NSDictionary, tag: Int) {
        self.view.endEditing(true)
        if tag == 3 {
            txtSelectCategory.text = (dictData.value(forKey: "TaskCategoryName")as! String)
            txtSelectCategory.tag = Int("\(dictData.value(forKey: "TaskCategoryId")!)")!
            self.arySubCategory = NSMutableArray()
            self.arySubCategory = (dictData.value(forKey: "SubCategory")as! NSArray).mutableCopy()as! NSMutableArray
            self.txtSelectSubCategory.text = ""
            self.txtSelectSubCategory.tag = 0
        }else if (tag == 4){
            txtSelectSubCategory.text = (dictData.value(forKey: "TaskSubCategoryName")as! String)
            txtSelectSubCategory.tag = Int("\(dictData.value(forKey: "TaskSubCategoryId")!)")!
        }else if (tag == 0){
            print(dictData)
            self.aryTaskList.add(dictData)
            self.tvSubTaskList.reloadData()
        }
    }
}
