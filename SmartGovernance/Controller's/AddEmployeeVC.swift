//
//  AddEmployeeVC.swift
//  SmartGovernance
//
//  Created by Navin Patidar on 3/7/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
import ContactsUI
import Contacts

class AddEmployeeVC: UIViewController , CNContactPickerDelegate{
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var txtEmployeeName: ACFloatingTextfield!
    @IBOutlet weak var txtMobileNumber: ACFloatingTextfield!
    @IBOutlet weak var txtEmailID: ACFloatingTextfield!
    @IBOutlet weak var txtDesignation: ACFloatingTextfield!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        buttonRounded(sender: btnSave)
    }
    

    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionOnSave(_ sender: UIButton) {
        self.view.endEditing(true)
        if(validation()){
            callAddEmployeeAPI(sender: sender)
        }
    }
    @IBAction func actionOnContact(_ sender: UIButton) {
        self.view.endEditing(true)
        let cnPicker = CNContactPickerViewController()
        cnPicker.delegate = self
        self.present(cnPicker, animated: true, completion: nil)
    }
    // MARK: - -------------CNContactPickerViewController
    // MARK: -
    
  
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        let userName:String = contact.givenName  +  " " + contact.familyName
        txtEmployeeName.text = ""
        txtMobileNumber.text = ""
        txtEmailID.text = ""

        txtEmployeeName.text = userName
        // user phone number
        for number in contact.phoneNumbers {
            let phoneNumber = number.value
            print("number is = \(phoneNumber)")
            txtMobileNumber.text = phoneNumber.stringValue
        }
        
        if(((contact.emailAddresses.first?.value as String?)) != nil){
            let email:String = (contact.emailAddresses.first?.value as String?)!
            txtEmailID.text = email
            if(txtEmailID.text == "[]"){
                txtEmailID.text = ""
            }
        }
        
    }
    
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        print("Cancel Contact Picker")
    }
    // MARK: - ---------------API Calling
    // MARK: -
    func callAddEmployeeAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
      
            let dictData = NSMutableDictionary()
            dictData.setValue("0", forKey: "TMEmpId")
            dictData.setValue("\(dict_Login_Data.value(forKey: "EmpId")!)", forKey: "AssignByEmpId")
            dictData.setValue("\(txtEmployeeName.text!)", forKey: "TMEmpName")
            dictData.setValue("\(txtMobileNumber.text!)", forKey: "TMEmpMobileNo")
            dictData.setValue("\(txtEmailID.text!)", forKey: "TMEmpEmailId")
            dictData.setValue("\(txtDesignation.text!)", forKey: "TMEmpDesignation")
           
            let arySendData = NSMutableArray()
            arySendData.add(dictData)
            var json = Data()
            var jsonString = NSString()
            if JSONSerialization.isValidJSONObject(arySendData) {
                // Serialize the dictionary
                json = try! JSONSerialization.data(withJSONObject: arySendData, options: .prettyPrinted)
                jsonString = String(data: json, encoding: .utf8)! as NSString
                print("UpdateLeadinfo JSON: \(jsonString)")
            }
            if(arySendData.count == 0){
                jsonString = ""
            }
            
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AddTaskManagementEmployee xmlns='\(BaseURL0)'><AddEmpMasterDc>\(jsonString)</AddEmpMasterDc></AddTaskManagementEmployee></soap12:Body></soap12:Envelope>"
            
            dotLoader(sender: sender , controller : self, viewContain: self.view, onView: "Button")
            
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "AddTaskManagementEmployeeResult", responcetype: "AddTaskManagementEmployeeResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        FTIndicator.showNotification(withTitle: alertInfo, message: "Employee Added Successfully!")
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    // MARK: - ---------------validation
    // MARK: -
    func validation() -> Bool {

        if(txtEmployeeName.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertEmployeeName, viewcontrol: self)
            return false
        }else if (txtMobileNumber.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertMobileNumber, viewcontrol: self)
            return false
            
        }else if ((txtMobileNumber.text?.count)! < 10){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertMobileLimit, viewcontrol: self)
            return false
        }
        else if (txtEmailID.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertEmail, viewcontrol: self)
            return false
            
        }else if !(validateEmail(email: txtEmailID.text!)){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertEmailValid, viewcontrol: self)
            return false
            
        } else if (txtDesignation.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDesignation, viewcontrol: self)
            return false
        }
        return true
    }
    
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension AddEmployeeVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtEmployeeName   || textField == txtDesignation   || textField == txtEmailID {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 45)
        }
        if textField == txtMobileNumber  {
            return (txtFiledValidation(textField: txtMobileNumber, string: string, returnOnly: "NUMBER", limitValue: 9))
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
}
