//
//  MyProfileVC.swift
//  SmartGovernance
//
//  Created by Navin Patidar on 3/18/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class MyProfileVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var txtUserName: ACFloatingTextfield!
    @IBOutlet weak var txtMobile: ACFloatingTextfield!
    @IBOutlet weak var txtEmail: ACFloatingTextfield!
    @IBOutlet weak var txtDesignation: ACFloatingTextfield!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var imgViewBackground: UIImageView!
    @IBOutlet weak var btnChangePass: UIButton!
  
    
    @IBOutlet var viewChangePassword: UIView!
    
    
    @IBOutlet weak var txtOldPassword: ACFloatingTextfield!
    @IBOutlet weak var txtNewPassword: ACFloatingTextfield!
    @IBOutlet weak var txtConfirmPassword: ACFloatingTextfield!
    @IBOutlet weak var btnSubmit: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
    buttonRounded(sender: btnChangePass)
        txtUserName.text =  "\(dict_Login_Data.value(forKey: "Name")!) "
        txtDesignation.text =  "\(dict_Login_Data.value(forKey: "Designation")!) "
      txtMobile.text =  "\(dict_Login_Data.value(forKey: "MobileNo")!) "
        txtEmail.text =  "\(dict_Login_Data.value(forKey: "EmailId")!) "
       imgView.layer.borderWidth = 1.0
        imgView.layer.masksToBounds = false
        imgView.layer.cornerRadius = imgView.frame.height/2
        imgView.clipsToBounds = true
        imgView.layer.borderColor = UIColor.white.cgColor
        imgView.backgroundColor = hexStringToUIColor(hex: primaryColor)
        let urlImage = "\(BaseURLSplash)\(dict_Login_Data.value(forKey: "ProfileImage")!)"
       imgView.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: "no-image"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            print(url ?? 0)
        }, usingActivityIndicatorStyle: .gray)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
              let urlImage = "\(BaseURLSplash)\(dict_Login_Data.value(forKey: "ProfileImage")!)"
           self.imgViewBackground.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: "no-image"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                print(url ?? 0)
            }, usingActivityIndicatorStyle: .gray)
            let blurEffect = UIBlurEffect(style: .extraLight )
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.imgViewBackground.frame
            self.imgViewBackground.insertSubview(blurEffectView, at: 0)
        }
      
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func actionOnChangePassword(_ sender: UIButton) {
        self.view.endEditing(true)
        viewChangePassword.frame = CGRect(x: 0, y: -self.view.frame.height, width: self.view.frame.width, height: self.view.frame.height)
        UIView.animate(withDuration: 0.5) {
            self.viewChangePassword.frame = CGRect(x:0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        }
        self.view.addSubview(self.viewChangePassword)
    }
    
    
    @IBAction func actionOnCross(_ sender: UIButton) {
 
        UIView.animate(withDuration: 0.5) {
            self.viewChangePassword.frame = CGRect(x:0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
              self.viewChangePassword.removeFromSuperview()
        }
    }
    
    @IBAction func actionOnSubmit(_ sender: UIButton) {
        if(validation()){
            self.callChangePasswordAPI(sender: sender)
        }
    }
    
    // MARK: - ---------------API Calling
    // MARK: -
    func callChangePasswordAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><ChangePassword xmlns='\(BaseURL0)'><username>\(dict_Login_Data.value(forKey: "UserName")!)</username><oldpassword>\(txtOldPassword.text!)</oldpassword><newpassword>\(txtNewPassword.text!)</newpassword></ChangePassword></soap12:Body></soap12:Envelope>"
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "ChangePasswordResult", responcetype: "ChangePasswordResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        FTIndicator.showNotification(withTitle: alertInfo, message: "\(dictTemp.value(forKey: "Msg")!)")
                        self.viewChangePassword.removeFromSuperview()
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    // MARK: - ---------------validation
    // MARK: -
    func validation() -> Bool {
        if(txtOldPassword.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_OldPassword, viewcontrol: self)
            return false
        }
     else if (txtNewPassword.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertPassword, viewcontrol: self)
            return false
        }else if (txtConfirmPassword.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCPassword, viewcontrol: self)
            return false
        }else if (txtNewPassword.text! != txtConfirmPassword.text!){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_CPasswordmatch, viewcontrol: self)
            return false
        }
        return true
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -

extension MyProfileVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtOldPassword   || textField == txtNewPassword || textField == txtConfirmPassword {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 50)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
}
