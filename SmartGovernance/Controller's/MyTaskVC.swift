//
//  MyTaskVC.swift
//  SmartGovernance
//
//  Created by Navin Patidar on 3/7/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class MyTaskVC: UIViewController {
    
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var tvList: UITableView!
    @IBOutlet weak var viewScrollContain: CardView!

    var array_btnMenu = NSMutableArray()
    var arrayDatalist = NSMutableArray()
    var arrayTvlist = NSMutableArray()
    var arrayEmployeelist = NSMutableArray()
    
    var arrayOverDuelist = NSMutableArray()
    var arrayDuelist = NSMutableArray()
    var arrayActionTakenlist = NSMutableArray()
    var arrayCompletedlist = NSMutableArray()
  var showColor = UIColor.gray
    
    
    var strButtonPossition : Int!
    @IBOutlet weak var viewHeader: CardView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tvList.tableFooterView = UIView()
        tvList.estimatedRowHeight = 80.0
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        array_btnMenu.add("ALL")
        array_btnMenu.add("OVER DUE")
        array_btnMenu.add("DUE")
        array_btnMenu.add("ACTION TAKEN")
        array_btnMenu.add("COMPLETED")
        array_btnMenu.add("EMPLOYEE")
        strButtonPossition = 0
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        set_UPMenuTopBarButtons()
 
        scroll.frame = CGRect(x: 0, y: 0, width: self.viewScrollContain.frame.width, height: self.viewScrollContain.frame.height)
        callMyTaskAPI(sender: UIButton())
        callMyTaskEmployeeAPI(sender: UIButton())
    }
    
    override func viewWillLayoutSubviews() {
        tvList.tableFooterView = UIView()
        tvList.estimatedRowHeight = 80.0
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
  
    // MARK: - ---------------scrollView Delegate
    // MARK: -
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
      
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
       
    }
    
    // MARK: - ---------------UPMenuTopBarButtons
    // MARK: -
    func set_UPMenuTopBarButtons(){
        arrayTvlist = NSMutableArray()
        let subViews = self.scroll.subviews
        for subview in subViews{
            subview.removeFromSuperview()
        }
        self.view.endEditing(true)
        var iWidthButton: CGFloat = 0.0
        var totalx: CGFloat = 0.0
        for i in 0..<array_btnMenu.count {
            let btn_Menu = UIButton()
            let lblIndicate = UILabel()
            let strTitle = array_btnMenu[i]as! String
            btn_Menu.setTitle(strTitle as String?, for: .normal)
            btn_Menu.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
            let maxLabelSize: CGSize = CGSize(width: 300,height: CGFloat(9999))
            let contentNSString = strTitle as NSString
            let expectedLabelSize = contentNSString.boundingRect(with: maxLabelSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14.0)], context: nil)
            iWidthButton = expectedLabelSize.size.width + 40
            btn_Menu.frame = CGRect(x: totalx, y: 0, width: iWidthButton - 1, height:scroll.frame.size.height)
            lblIndicate.frame = CGRect(x: totalx, y: scroll.frame.size.height - 2  , width: iWidthButton, height:scroll.frame.size.height)
            totalx = totalx + iWidthButton
            self.scroll.layer.cornerRadius = 0.0
            btn_Menu.tag = i
            lblIndicate.tag = i
            btn_Menu.addTarget(self, action: #selector(self.action_ChangeMenu), for: .touchUpInside)
            lblIndicate.backgroundColor = UIColor.clear

            if(i == 0){
                btn_Menu.setTitleColor(UIColor.darkGray, for: UIControl.State.normal)
                lblIndicate.backgroundColor = UIColor.darkGray
                btn_Menu.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold)

            }else if(i == 1) {
                btn_Menu.setTitleColor(UIColor.red, for: UIControl.State.normal)
            
            }else if(i == 2) {
                btn_Menu.setTitleColor(hexStringToUIColor(hex: primaryOrangeColor), for: UIControl.State.normal)
            }else if(i == 3) {
                btn_Menu.setTitleColor(hexStringToUIColor(hex: primaryBlueColor), for: UIControl.State.normal)
            }else if(i == 4) {
                btn_Menu.setTitleColor(hexStringToUIColor(hex: primaryGreenColor), for: UIControl.State.normal)
            }else if(i == 5) {
                btn_Menu.setTitleColor(UIColor.black, for: UIControl.State.normal)
            }
            scroll.addSubview(btn_Menu)
            scroll.addSubview(lblIndicate)
        }
        UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions.curveLinear, animations: {
            self.scroll.contentSize  = CGSize(width: totalx, height: self.scroll.frame.size.height)
            if self.strButtonPossition == 0{
                self.scroll.contentOffset.x = CGFloat(self.strButtonPossition)
            }else{
                self.scroll.contentOffset.x = CGFloat(self.strButtonPossition - 100)
            }
        }, completion: nil)
        self.view.layoutIfNeeded()
        self.viewWillLayoutSubviews()
        self.setUpTableData(tag: 0)
    }
    @objc func action_ChangeMenu(sender: AnyObject) {
        let subviews : NSArray = self.scroll.subviews as NSArray
        for btn_Menu in subviews{
            if (btn_Menu is UIButton) {
                if (btn_Menu as! UIButton).tag == sender.tag {
                    (btn_Menu as AnyObject).titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold)
                    setUpTableData(tag: sender.tag)
                }
                else {
                    (btn_Menu as AnyObject).titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
                }
            }
            if (btn_Menu is UILabel) {
                if (btn_Menu as! UILabel).tag == sender.tag {
                    ((btn_Menu as AnyObject)as! UILabel).backgroundColor = UIColor.darkGray
                }
                else {
                   ((btn_Menu as AnyObject)as! UILabel).backgroundColor = UIColor.clear
                }
            }
        }
        
        
        
    }
    func setUpTableData(tag : Int){
      tvList.tag = tag
        print(tag)
        
        self.arrayTvlist = NSMutableArray()
        if(tag == 0){
             self.arrayTvlist = self.arrayDatalist
            showColor = UIColor.gray
            print(self.arrayTvlist.count)
        }else if(tag == 1){
            showColor = UIColor.red
           self.arrayTvlist =  arrayOverDuelist
        }else if(tag == 2){
            self.arrayTvlist = arrayDuelist
            showColor = hexStringToUIColor(hex: primaryOrangeColor)
       }else if(tag == 3){
            self.arrayTvlist = arrayActionTakenlist
            showColor = hexStringToUIColor(hex: primaryBlueColor)

        }else if(tag == 4){
            self.arrayTvlist = arrayCompletedlist
              showColor = hexStringToUIColor(hex: primaryGreenColor)
        }else if(tag == 5){
          self.arrayTvlist = arrayEmployeelist
        }
        if(arrayTvlist.count == 0){
            self.showErrorWithImage(strTitle: alertDataNotFound, imgError: strDataNotFoundImage)
        }else{
          removeErrorView()
        }
        self.tvList.reloadData()
    }
    // MARK: - ---------------FilterData
    // MARK: -
    func filterAccordingToStatus(arryData : NSMutableArray){
       arrayCompletedlist = NSMutableArray()
        arrayActionTakenlist = NSMutableArray()
        arrayDuelist = NSMutableArray()
        arrayOverDuelist = NSMutableArray()

        for item in arryData {
            print(item)
            let dict = (item as AnyObject)as! NSDictionary
             let aryTaskData = (dict.value(forKey: "SubTask")as! NSArray)
            var strStatus = ""
                for item1 in aryTaskData {
                    print(item1)
                    let dict = (item1 as AnyObject)as! NSDictionary
                    let DueDate = "\(dict.value(forKey: "DueDate")!) "
                    let CurrentDate = getTodayDateString(formet: "MM/dd/yyyy")
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss a"
                    let strDueDateTemp = dateFormatter.date(from:DueDate)!
                    dateFormatter.dateFormat = "MM/dd/yyyy"
                    let somedateString = dateFormatter.string(from: strDueDateTemp)
                    let strDueDate = dateFormatter.date(from:somedateString)!
                    let strCurrentDate = dateFormatter.date(from:CurrentDate)!
                    if(strDueDate < strCurrentDate) &&  "\(dict.value(forKey: "Status")!)" != "Completed"{
                              arrayOverDuelist.add(item)
                            break
                    }else{
                        strStatus = strStatus + "\(dict.value(forKey: "Status")!)"
                    }
             }
            if(strStatus.contains("Due")){
                arrayDuelist.add(item)
            }else if(strStatus.contains("ActionTaken")){
                arrayActionTakenlist.add(item)
            }else if(strStatus.contains("Completed")){
                arrayCompletedlist.add(item)
            }
        }
    }
    
    // MARK: - ---------------API Calling
    // MARK: -
    func callMyTaskAPI(sender : UIButton) {
        if !isInternetAvailable() {
            self.showErrorWithImage(strTitle: alertInternet, imgError: strInternetImage)
        }else{
            let dictData = NSMutableDictionary()
//"\(dict_Login_Data.value(forKey: "EmpId")!)"
            dictData.setValue("\(dict_Login_Data.value(forKey: "EmpId")!)", forKey: "AssignByEmpId")
            dictData.setValue("0", forKey: "TaskCategoryId")
            dictData.setValue("0", forKey: "TaskSubCategoryId")
            dictData.setValue("", forKey: "TaskName")
            let arySendData = NSMutableArray()
            arySendData.add(dictData)
            var json = Data()
            var jsonString = NSString()
            if JSONSerialization.isValidJSONObject(arySendData) {
                // Serialize the dictionary
                json = try! JSONSerialization.data(withJSONObject: arySendData, options: .prettyPrinted)
                jsonString = String(data: json, encoding: .utf8)! as NSString
                print("UpdateLeadinfo JSON: \(jsonString)")
            }
            if(arySendData.count == 0){
                jsonString = ""
            }
            
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetTaskManagement xmlns='\(BaseURL0)'><SearchJson>\(jsonString)</SearchJson></GetTaskManagement></soap12:Body></soap12:Envelope>"
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetTaskManagementResult", responcetype: "GetTaskManagementResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        let aryTemp = dictTemp.value(forKey: "Task")as! NSArray
                        self.arrayDatalist = NSMutableArray()
                        self.arrayDatalist = aryTemp.mutableCopy()as! NSMutableArray
                        self.filterAccordingToStatus(arryData: self.arrayDatalist)
                        self.setUpTableData(tag: 0)
                    }else{
                       self.showErrorWithImage(strTitle: alertDataNotFound, imgError: strDataNotFoundImage)
                    }
                }else{
                    self.showErrorWithImage(strTitle: alertSomeError, imgError: strDataNotFoundImage)
                }
            }
        }
    }
    func callMyTaskEmployeeAPI(sender : UIButton) {
        if !isInternetAvailable() {
            self.showErrorWithImage(strTitle: alertInternet, imgError: strInternetImage)
        }else{
           // \(dict_Login_Data.value(forKey: "EmpId")!)
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetEmpSubTaskCountSummary xmlns='\(BaseURL0)'><StartDate></StartDate><DueDate></DueDate><AssignByEmpId>\(dict_Login_Data.value(forKey: "EmpId")!)</AssignByEmpId></GetEmpSubTaskCountSummary></soap12:Body></soap12:Envelope>"
         //   dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetEmpSubTaskCountSummaryResult", responcetype: "GetEmpSubTaskCountSummaryResponse") { (responce, status) in
              //  remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        let aryTemp = dictTemp.value(forKey: "EmpCountSummary")as! NSArray
                        self.arrayEmployeelist = NSMutableArray()
                        self.arrayEmployeelist = aryTemp.mutableCopy()as! NSMutableArray
                    }else{
                      //  self.showErrorWithImage(strTitle: alertDataNotFound, imgError: strDataNotFoundImage)
                    }
                }else{
                   // self.showErrorWithImage(strTitle: alertSomeError, imgError: strDataNotFoundImage)
                }
            }
        }
    }
    
    
    // MARK: - --------------Show Error / Network
    // MARK: -
    
    func showErrorWithImage( strTitle : String , imgError : UIImage)   {
        removeErrorView()
        addErrorImageOnView(strMessage: strTitle, img: imgError, controller: self, view: viewScrollContain)
        btnRetry.addTarget(self, action: #selector(sayAction(_:)), for: .touchUpInside)
    }
    @objc private func sayAction(_ sender: UIButton!) {
        removeErrorView()
        self.viewWillAppear(true)
    }
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension MyTaskVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return arrayTvlist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tvList.tag == 5 {
            let cell = tvList.dequeueReusableCell(withIdentifier: "EmployeeCell", for: indexPath as IndexPath) as! EmployeeCell
           let dict = arrayTvlist.object(at: indexPath.row)as! NSDictionary
            cell.lblName.text = "\(dict.value(forKey: "empname")!)"
            cell.lblTotalCount.text = "\(dict.value(forKey: "total")!)\nTotal Task"
            cell.lblOverDue.text = "Over Due : \(dict.value(forKey: "overdue")!) "
            cell.lblDue.text = "\(dict.value(forKey: "due")!) "
            cell.lblActiontaken.text = "\(dict.value(forKey: "actiontaken")!) "
            cell.lblCompleted.text = "\(dict.value(forKey: "completed")!) "

            return cell
        }else{
            let cell = tvList.dequeueReusableCell(withIdentifier: "MyTaskCell", for: indexPath as IndexPath) as! MyTaskCell
            let dict = arrayTvlist.object(at: indexPath.row)as! NSDictionary
            cell.lblTitle.text = "Task Name : \(dict.value(forKey: "TaskName")!) "
            cell.lblDetailTitle.text = "Description : \(dict.value(forKey: "TaskDesc")!) "
            cell.lblShowColor.backgroundColor = showColor
            return cell
        }
       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                 return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      if tvList.tag != 5 {
       
        let dict = arrayTvlist.object(at: indexPath.row)as! NSDictionary
        if((dict.value(forKey: "SubTask")as! NSArray).count != 0){
             let testController = mainStoryboard.instantiateViewController(withIdentifier: "MyTaskDetailVC")as! MyTaskDetailVC
            testController.dictData = (arrayTvlist.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary
            self.navigationController?.pushViewController(testController, animated: true)
        }else{
            FTIndicator.showToastMessage("Sub task not available!")
        }
    }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
                cell.alpha = 0.4
                cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
                UIView.animate(withDuration: 1.0) {
                    cell.alpha = 1
                    cell.transform = .identity
                }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
}

// MARK: - ----------------UserMy TaskCell
// MARK: -
class MyTaskCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetailTitle: UILabel!
    @IBOutlet weak var lblShowColor: UILabel!

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: - ----------------UserEmployee
// MARK: -
class EmployeeCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTotalCount: UILabel!
    @IBOutlet weak var lblOverDue: UILabel!
    @IBOutlet weak var lblDue: UILabel!
    @IBOutlet weak var lblActiontaken: UILabel!
    @IBOutlet weak var lblCompleted: UILabel!

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
