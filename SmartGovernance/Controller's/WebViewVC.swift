//
//  WebViewVC.swift
//  Aahar
//
//  Created by Navin Patidar on 2/25/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class WebViewVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var viewWeb: UIWebView!
    @IBOutlet weak var lblTitle: UILabel!
    var strViewComeFrom = ""
    var webURL = ""

    override func viewDidLoad() {
         super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
         lblTitle.text = strViewComeFrom
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if !(isInternetAvailable()){
            showErrorWithImage(strTitle: alertInternet, imgError: strInternetImage)
        }else{
            viewWeb.loadRequest(URLRequest(url: URL(string: webURL)!))
            viewWeb.scrollView.maximumZoomScale = 8
            viewWeb.scrollView.minimumZoomScale = 1
            viewWeb.delegate = self
            
        }
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    // MARK: - --------------Show Error / Network
    // MARK: -
    
    func showErrorWithImage( strTitle : String , imgError : UIImage)   {
        removeErrorView()
        addErrorImageOnView(strMessage: strTitle, img: imgError, controller: self, view: viewHeader)
        btnRetry.addTarget(self, action: #selector(sayAction(_:)), for: .touchUpInside)
    }
    @objc private func sayAction(_ sender: UIButton!) {
        removeErrorView()
        self.viewWillAppear(true)
    }
    
}
// MARK :
//MARK : UIWebViewDelegate
extension WebViewVC : UIWebViewDelegate{
    func webViewDidStartLoad(_ webView: UIWebView) {
        dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "MainView")
        
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        remove_dotLoader(controller: self)
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        remove_dotLoader(controller: self)
    }
}
