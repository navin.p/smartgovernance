//
//  RegistrationVC.swift
//  SmartGovernance
//
//  Created by Navin Patidar on 3/15/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class RegistrationVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var viewLogIn: CardView!
    @IBOutlet weak var txtUserName: ACFloatingTextfield!
    @IBOutlet weak var txtFullName: ACFloatingTextfield!
    
    @IBOutlet weak var txtPassword: ACFloatingTextfield!
    @IBOutlet weak var txtCPassword: ACFloatingTextfield!
    @IBOutlet weak var txtMobile: ACFloatingTextfield!
    @IBOutlet weak var txtEmail: ACFloatingTextfield!
    @IBOutlet weak var txtDesignation: ACFloatingTextfield!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var imgViewSplash: UIImageView!
    var imagePicker = UIImagePickerController()
    var strProfileImageName = ""
    var strSplashImageName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        let blurEffect = UIBlurEffect(style: .extraLight)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame
        
        self.imgViewSplash.insertSubview(blurEffectView, at: 0)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        imgView.layer.borderWidth = 1.0
        imgView.layer.masksToBounds = false
        imgView.layer.cornerRadius = imgView.frame.height/2
        imgView.clipsToBounds = true
        imgView.layer.borderColor = UIColor.white.cgColor
        self.imgView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration:1.5,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.imgView.transform = .identity
            },
                       completion: nil)
        
    }
    
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actionOnLogin(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionOnSignUp(_ sender: UIButton) {
        self.view.endEditing(true)
        if(validation()){
            if(strProfileImageName.count  == 0){
                self.callRegistrationAPI(sender: sender)
            }else {
                self.uploadProfileImage()
            }
        }
    }
    
    // MARK: - ---------------validation
    // MARK: -
    func validation() -> Bool {
        if(txtFullName.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertFullName, viewcontrol: self)
            return false
        }
        else if(txtUserName.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertUserName, viewcontrol: self)
            return false
        }else if(txtDesignation.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_DesignationUser, viewcontrol: self)
            return false
        }
        else if (txtEmail.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertEmail, viewcontrol: self)
            return false
            
        }else if !(validateEmail(email: txtEmail.text!)){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertEmailValid, viewcontrol: self)
            return false
            
        }else if (txtMobile.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertMobileNumber, viewcontrol: self)
            return false
            
        }else if ((txtMobile.text?.count)! < 10){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertMobileLimit, viewcontrol: self)
            return false
        } else if (txtPassword.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertPassword, viewcontrol: self)
            return false
        }else if (txtCPassword.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCPassword, viewcontrol: self)
            return false
        }else if (txtCPassword.text! != txtPassword.text!){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_CPasswordmatch, viewcontrol: self)
            return false
        }
        return true
    }
    // MARK: - ---------------API Calling
    // MARK: -
    
    func uploadProfileImage()  {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            WebService.callAPIWithImage(parameter: NSDictionary(), url: BaseURLUploadProfile, image: self.imgView.image!, fileName: strProfileImageName, withName: "userfile") { (responce, status) in
                remove_dotLoader(controller: self)
                if(status == "Suceess"){
                    self.callRegistrationAPI(sender: UIButton())
                }
            }
        }
    }
    
    func callRegistrationAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AddEmployeeRegistration xmlns='\(BaseURL0)'><EmpId>0</EmpId><Name>\(txtFullName.text!)</Name><EmailId>\(txtEmail.text!)</EmailId><MobileNo>\(txtMobile.text!)</MobileNo><Designation>\(txtDesignation.text!)</Designation><UserName>\(txtUserName.text!)</UserName><Password>\(txtPassword.text!)</Password><Role>management</Role><ProfileImage>\(self.strProfileImageName)</ProfileImage><SplashScreen>\(self.strSplashImageName)</SplashScreen></AddEmployeeRegistration></soap12:Body></soap12:Envelope>"
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "AddEmployeeRegistrationResult", responcetype: "AddEmployeeRegistrationResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        FTIndicator.showNotification(withTitle: alertInfo, message: "\(dictTemp.value(forKey: "Msg")!)")
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    @IBAction func actionOnEditProfileImage(_ sender: UIButton) {
        self.view.endEditing(true)
        let alert = UIAlertController(title: Alert_SelectionMessage, message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: Alert_Gallery, style: .default , handler:{ (UIAlertAction)in
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .savedPhotosAlbum;
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        alert.addAction(UIAlertAction(title: Alert_Camera, style: .default , handler:{ (UIAlertAction)in
            if(UIImagePickerController .isSourceTypeAvailable(.camera)){
                self.imagePicker.sourceType = .camera
                self.imagePicker.delegate = self
                self.present(self.imagePicker, animated: true, completion: nil)
            } else {
            }
        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
        }))
        self.present(alert, animated: true, completion: {
        })
    }
    
}
// MARK: - ----------------UIImagePickerControllerDelegate
// MARK: -

extension RegistrationVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage {
            self.imgView.contentMode = .scaleAspectFill
            self.imgView.image = image
            self.strProfileImageName = getUniqueString()
            self.dismiss(animated: false) {
            }
        } else {
            self.dismiss(animated: false) {
            }
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  false, completion: nil)
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -

extension RegistrationVC : UITextFieldDelegate{
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtUserName   || textField == txtDesignation || textField == txtFullName {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 50)
        }
        if textField == txtMobile  {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 9)
        }
        if textField == txtPassword ||  textField == txtCPassword  || textField == txtEmail{
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 35)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
}
