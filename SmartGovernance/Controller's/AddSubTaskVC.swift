//
//  AddSubTaskVC.swift
//  SmartGovernance
//
//  Created by Navin Patidar on 3/7/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class AddSubTaskVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var btnSave: UIButton!
    
    @IBOutlet weak var txtTaskName: ACFloatingTextfield!
    @IBOutlet weak var txtDescription: KMPlaceholderTextView!
    @IBOutlet weak var txtDueDate: ACFloatingTextfield!
    @IBOutlet weak var txtStartDate: ACFloatingTextfield!
    @IBOutlet weak var txtSelectPriority: ACFloatingTextfield!
    @IBOutlet weak var txtSelectStatus: ACFloatingTextfield!
    @IBOutlet weak var txtAssignTO : ACFloatingTextfield!

    
    @IBOutlet var viewPicker: UIView!
    @IBOutlet weak var picker: UIDatePicker!
    @IBOutlet weak var btnDone: UIButton!
    
    weak var handle_AddTaskScreen : AddTaskScreenDelegate?

    
    var aryAssignselected = NSMutableArray()
    var aryAssign = NSMutableArray()
    var strSubCategoryID = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        btnDone.backgroundColor = hexStringToUIColor(hex: primaryColor)

        buttonRounded(sender: btnSave)
        callGetCategoryWiseEmployeeAPI() 
        // Do any additional setup after loading the view.
    }
    
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionOnSelectDate(_ sender: UIButton) {
        self.view.endEditing(true)
        self.viewPicker.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        picker.minimumDate = Date()
        picker.tag = sender.tag
        self.view.addSubview(self.viewPicker)
    }
    @IBAction func actionOnSelectPriority(_ sender: UIButton) {
        sender.tag = 5
        self.view.endEditing(true)
        let aryPriority = NSMutableArray()
        let dict = NSMutableDictionary()
        dict.setValue("Low", forKey: "Name")
        dict.setValue("1", forKey: "id")
       
        let dict1 = NSMutableDictionary()
        dict1.setValue("Medium", forKey: "Name")
        dict1.setValue("2", forKey: "id")
       
        let dict2 = NSMutableDictionary()
        dict2.setValue("High", forKey: "Name")
        dict2.setValue("3", forKey: "id")
      
        aryPriority.add(dict)
        aryPriority.add(dict1)
        aryPriority.add(dict2)


        gotoPopViewWithArray(sender: sender, aryData: aryPriority, strTitle: "\(txtSelectPriority.placeholder!)")
    }
    @IBAction func actionOnSelectStatus(_ sender: UIButton) {
        sender.tag = 6
        self.view.endEditing(true)
        
        let aryStatus = NSMutableArray()
        let dict = NSMutableDictionary()
        dict.setValue("Due", forKey: "Name")
        dict.setValue("1", forKey: "id")
        
        let dict1 = NSMutableDictionary()
        dict1.setValue("ActionTaken", forKey: "Name")
        dict1.setValue("2", forKey: "id")
        
        let dict2 = NSMutableDictionary()
        dict2.setValue("Completed", forKey: "Name")
        dict2.setValue("3", forKey: "id")
        
        aryStatus.add(dict)
        aryStatus.add(dict1)
        aryStatus.add(dict2)
        gotoPopViewWithArray(sender: sender, aryData: aryStatus, strTitle: "\(txtSelectStatus.placeholder!)")
        
    }
    @IBAction func actionOnAssignTo(_ sender: UIButton) {
        sender.tag = 7
        self.view.endEditing(true)
        if(self.aryAssign.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertEmployeeList, viewcontrol: self)
        }else{
            gotoPopViewWithArray(sender: sender, aryData: aryAssign, strTitle: "\(txtAssignTO.placeholder!)")
        }
    }
    @IBAction func actionOnSave(_ sender: UIButton) {
        
        self.view.endEditing(true)
        if(validation() ){
            let dictSend = NSMutableDictionary()
            let aryAddEmp = NSMutableArray()
            for item in aryAssignselected{
                let dict = NSMutableDictionary()
                dict.setValue("0", forKey: "TaskManagementId")
                dict.setValue("0", forKey: "TMAssignEmpId")
                dict.setValue("\((item as AnyObject).value(forKey: "TMEmpId")!)", forKey: "TMEmpId")
                aryAddEmp.add(dict)
            }
            dictSend.setValue(aryAddEmp, forKey: "AssignEmploye")
            dictSend.setValue("0", forKey: "TaskManagementId")
            dictSend.setValue("0", forKey: "SubTaskManagementId")
            dictSend.setValue(txtTaskName.text!, forKey: "SubTaskName")
            dictSend.setValue(txtDescription.text!, forKey: "SubTaskDesc")
            dictSend.setValue(txtSelectStatus.text!, forKey: "Status")
            dictSend.setValue(txtStartDate.text!, forKey: "StartDate")
            dictSend.setValue(txtSelectPriority.text!, forKey: "Priority")
            dictSend.setValue(txtDueDate.text!, forKey: "DueDate")
            handle_AddTaskScreen?.AddTaskScreenScreen(dictData: dictSend, tag: 0)
            self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func actionONDone(_ sender: UIButton) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let somedateString = dateFormatter.string(from: picker.date)
        if picker.tag == 1 {
            txtStartDate.text = somedateString
        }else{
            txtDueDate.text = somedateString
        }
        if(txtStartDate.text != "" && txtDueDate.text != ""){
            let strDueDate = dateFormatter.date(from:txtDueDate.text!)!
            let strStartDate = dateFormatter.date(from:txtStartDate.text!)!
            if(strStartDate > strDueDate){
                txtDueDate.text = ""
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDateCheck, viewcontrol: self)
            }
        }
        self.viewPicker.removeFromSuperview()
    }
    // MARK: - ---------------validation
    // MARK: -
    func validation() -> Bool {
   
        if(txtTaskName.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSubTaskName, viewcontrol: self)
            return false
        }
       else if (txtDescription.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSubTaskDescription, viewcontrol: self)
            return false
        } else if (txtStartDate.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertStartDate, viewcontrol: self)
            return false
        }else if (txtDueDate.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDueDate, viewcontrol: self)
            return false
        }else if (txtSelectPriority.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSubTaskPriority, viewcontrol: self)
            return false
        }else if (txtSelectStatus.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSubTaskStatus, viewcontrol: self)
            return false
        }else if (txtAssignTO.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSubTaskAssign, viewcontrol: self)
            return false
        }
        return true
    }
    // MARK: - ---------------API Calling
    // MARK: -
    func callGetCategoryWiseEmployeeAPI() {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }else{
            //
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetCategoryWiseEmployee xmlns='\(BaseURL0)'><SubCategoryId>\(strSubCategoryID)</SubCategoryId></GetCategoryWiseEmployee></soap12:Body></soap12:Envelope>"
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetCategoryWiseEmployeeResult", responcetype: "GetCategoryWiseEmployeeResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        self.aryAssign = NSMutableArray()
                        self.aryAssign = (dictTemp.value(forKey: "CategoryEmployee")as! NSArray).mutableCopy()as! NSMutableArray
                    }else{
                        //self.showErrorWithImage(strTitle: alertDataNotFound, imgError: strDataNotFoundImage)
                    }
                }else{
                    // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    // MARK: - ---------------PopUPView
    // MARK: -
    func gotoPopViewWithArray(sender: UIButton , aryData : NSMutableArray , strTitle: String)  {
        let vc: PopUpView = self.storyboard!.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = strTitle
        vc.strTag = sender.tag
        
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handleAddSubTaskScreen = self
            vc.aryTBL = aryData
            vc.arySelected = aryAssignselected
            self.present(vc, animated: false, completion: {})
        }
    }
    
}
//MARK:
//MARK: ---------------Protocol-----------------

protocol AddSubTaskScreenDelegate : class{
    func AddSubTaskScreen(dictData : NSDictionary ,tag : Int)
}
//MARK:-
//MARK:- ---------RegistrationScreenDelegate Methods----------

extension AddSubTaskVC: AddSubTaskScreenDelegate {
    func AddSubTaskScreen(dictData: NSDictionary, tag: Int) {
        self.view.endEditing(true)
        if tag == 5 {
            txtSelectPriority.text = (dictData.value(forKey: "Name")as! String)
            txtSelectPriority.tag = Int("\(dictData.value(forKey: "id")!)")!
        }else if (tag == 6){
            txtSelectStatus.text = (dictData.value(forKey: "Name")as! String)
            txtSelectStatus.tag = Int("\(dictData.value(forKey: "id")!)")!
        }
        else if (tag == 7){
            if(dictData.count != 0){
                let ary = dictData.value(forKey: "selected")as! NSArray
                var strName = ""
                for  item in ary{
                    strName = strName + "\((item as AnyObject).value(forKey: "TMEmpName")!),"
                }
                txtAssignTO.text = String(strName.dropLast())
                aryAssignselected = NSMutableArray()
                aryAssignselected = ary.mutableCopy()as! NSMutableArray
            }
        }
    }
}
