//
//  MyTaskDetailVC.swift
//  SmartGovernance
//
//  Created by Navin Patidar on 3/8/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class MyTaskDetailVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var tvList: UITableView!
    @IBOutlet weak var lblsubTaskCount: UILabel!
    var dictData = NSMutableDictionary()

    var array_TV = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        tvList.tableFooterView = UIView()
        tvList.estimatedRowHeight = 80.0
        array_TV = (dictData.value(forKey: "SubTask")as! NSArray).mutableCopy()as! NSMutableArray
        lblsubTaskCount.text = "Total Sub Task : \(array_TV.count)"
    }
    
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - ---------------API Calling
    // MARK: -
    func callChangeStatusAPI(tag : Int , strStatus : String , strSubId : String) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><UpdateSubTaskStatus xmlns='\(BaseURL0)'><SubTaskManagementId>\(strSubId)</SubTaskManagementId><Status>\(strStatus)</Status></UpdateSubTaskStatus></soap12:Body></soap12:Envelope>"
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "UpdateSubTaskStatusResult", responcetype: "UpdateSubTaskStatusResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        FTIndicator.showNotification(withTitle: alertInfo, message: "\(dictTemp.value(forKey: "Msg")!)")
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension MyTaskDetailVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_TV.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvList.dequeueReusableCell(withIdentifier: "MyTaskDetailCell", for: indexPath as IndexPath) as! MyTaskDetailCell
       let dict = array_TV.object(at: indexPath.row)as! NSDictionary
        cell.lblTitle.text = "\(dict.value(forKey: "SubTaskName")!)"
        cell.lblDetailTitle.text = "\(dict.value(forKey: "SubTaskDesc")!)"
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "MM/dd/yyyy HH:mm:ss a"
        let StartDatetemp = dateFormatter1.date(from:"\(dict.value(forKey: "StartDate")!)")!
        let DueDatetemp = dateFormatter1.date(from:"\(dict.value(forKey: "DueDate")!)")!
        dateFormatter1.dateFormat = "MM/dd/yyyy"
        let somedateString1 = dateFormatter1.string(from: StartDatetemp)
        let somedateString2 = dateFormatter1.string(from: DueDatetemp)
        cell.lblStartDate.text = "\(somedateString1)"
        cell.lblDueDate.text = "\(somedateString2)"
        //let StartDate = "\(dict.value(forKey: "StartDate")!) "
        let DueDate = "\(dict.value(forKey: "DueDate")!) "
        let CurrentDate = getTodayDateString(formet: "MM/dd/yyyy")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss a"
        let strDueDateTemp = dateFormatter.date(from:DueDate)!
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let somedateString = dateFormatter.string(from: strDueDateTemp)
        let strDueDate = dateFormatter.date(from:somedateString)!
        let strCurrentDate = dateFormatter.date(from:CurrentDate)!
        if(strDueDate < strCurrentDate) &&  "\(dict.value(forKey: "Status")!)" != "Completed"{
            cell.btnStatus.setTitle("OverDue", for: .normal)
            cell.btnStatus.setTitleColor(UIColor.red, for: .normal)
        }else{
            cell.btnStatus.setTitle("\(dict.value(forKey: "Status")!)", for: .normal)
            if("\(dict.value(forKey: "Status")!)" == "Due"){
                cell.btnStatus.setTitleColor(hexStringToUIColor(hex: primaryOrangeColor), for: .normal)
            }else if ("\(dict.value(forKey: "Status")!)" == "ActionTaken"){
                cell.btnStatus.setTitleColor(hexStringToUIColor(hex: primaryBlueColor), for: .normal)
            }else {
                cell.btnStatus.setTitleColor(hexStringToUIColor(hex: primaryGreenColor), for: .normal)
            }}
        cell.btnStatus.layer.cornerRadius = 4.0
        cell.btnStatus.layer.borderColor = cell.btnStatus.titleColor(for: .normal)?.cgColor
        cell.btnStatus.layer.borderWidth = 1.0
        cell.btnStatus.addTarget(self, action: #selector(pressButton(_:)), for: .touchUpInside)
        cell.btnStatus.tag = indexPath.row
        if(dict.value(forKey: "SubTaskEmployee") is NSArray){
            var strAssignUser = ""
            for item in (dict.value(forKey: "SubTaskEmployee") as! NSArray){
                strAssignUser = strAssignUser + "\((item as AnyObject).value(forKey: "TMEmpName")!),"
            }
            cell.lblAssignUser.text = "Assigned to - " + String(strAssignUser[..<strAssignUser.index(before: strAssignUser.endIndex)])
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
  
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
                cell.alpha = 0.4
                cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
                UIView.animate(withDuration: 1.0) {
                    cell.alpha = 1
                    cell.transform = .identity
                }
    }
func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

       @objc func pressButton(_ button: UIButton) {
        
        let dict = array_TV.object(at:(button.tag))as! NSDictionary
        let status = "\((button.titleLabel?.text!)!)"
        let id = "\(dict.value(forKey: "SubTaskManagementId")!)"
        
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select", message: "Option to change task status", preferredStyle: .actionSheet)

        let CancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(CancelActionButton)
        let DueActionButton = UIAlertAction(title: "Due", style: .default) { _ in
            self.callChangeStatusAPI(tag: 0, strStatus: "Due", strSubId: id)
        }
      
        
        let ActionButton = UIAlertAction(title: "ActionTaken", style: .default) { _ in
            self.callChangeStatusAPI(tag: 0, strStatus: "ActionTaken", strSubId: id)
        }
   
        let CompletedButton = UIAlertAction(title: "Completed", style: .default) { _ in
            self.callChangeStatusAPI(tag: 0, strStatus: "Completed", strSubId: id)
        }
        
        if(status != "Completed"){
            if(status == "ActionTaken" ){
                actionSheetControllerIOS8.addAction(DueActionButton)
                actionSheetControllerIOS8.addAction(CompletedButton)
            } else if(status == "Due" ){
                actionSheetControllerIOS8.addAction(ActionButton)
                actionSheetControllerIOS8.addAction(CompletedButton)
            }else if(status == "OverDue" ){
                actionSheetControllerIOS8.addAction(CompletedButton)
            }
            self.present(actionSheetControllerIOS8, animated: true, completion: nil)
        }
    }
}

// MARK: - ----------------UserDashBoardCell
// MARK: -
class MyTaskDetailCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetailTitle: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblDueDate: UILabel!
    @IBOutlet weak var btnStatus: UIButton!
    @IBOutlet weak var lblAssignUser: UILabel!

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
