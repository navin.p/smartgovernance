//
//  ViewBillVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/28/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class DrawerVC: UIViewController {
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var btnTransprant: UIButton!

    var aryList = NSMutableArray()
  weak var handleDrawerView: DrawerScreenDelegate?

    // MARK: - ---------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        btnTransprant.isHidden = true
        aryList = [["title":"Home","image":"home"],["title":"Profile","image":"add_category"],["title":"My Task","image":"add_category"],["title":"Add Category","image":"add_category"],["title":"Add Employee","image":"add_user"],["title":"Add Subcategory","image":"add_sub_category"],["title":"Disclaimer","image":"disclaimer"],["title":"Developer Info","image":"developer_info"],["title":"Logout","image":"logout"]]

        
        
        lblVersion.text = "Version - \(app_Version)"
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 80.0
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        tvlist.frame = CGRect(x: -self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        lblVersion.frame = CGRect(x: -self.view.frame.width, y: tvlist.frame.maxY, width: self.view.frame.width, height: 44)
                UIView.animate(withDuration: 0.5) {
                    self.tvlist.frame = CGRect(x:0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                    self.lblVersion.frame = CGRect(x: 0, y: self.tvlist.frame.maxY, width: self.view.frame.width, height: 44)
                    self.btnTransprant.isHidden = false
                }
    }
    @IBAction func actionOnBack(_ sender: UIButton) {
   //     handleDrawerView?.refreshDrawerScreen(strType: "", tag: 0)
        UIView.animate(withDuration: 0.5) {
            self.tvlist.frame = CGRect(x: -self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            self.dismiss(animated: false, completion: nil)
        }
        
    }
    @IBAction func actionOnVersion(_ sender: UIButton) {
        showAlertWithoutAnyAction(strtitle: alertInfo, strMessage:"App Version : \(app_Version)\nDate : \(app_VersionDate)\n\(app_VersionSupport)" , viewcontrol: self)
    }
}


// MARK: - ----------------UITableViewDelegate
// MARK: -

extension DrawerVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(section == 0){
            return 1
        }else{
            return aryList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            let cell = tvlist.dequeueReusableCell(withIdentifier: "Drawer1", for: indexPath as IndexPath) as! DrawerCell
            cell.lblName.text =  "\(dict_Login_Data.value(forKey: "Name")!)"
            cell.profileimage.layer.borderWidth = 1.0
            cell.profileimage.layer.masksToBounds = false
            cell.profileimage.layer.cornerRadius = cell.profileimage.frame.height/2
            cell.profileimage.clipsToBounds = true
            cell.profileimage.layer.borderColor = UIColor.white.cgColor
              cell.profileimage.backgroundColor = hexStringToUIColor(hex: primaryColor)
            let urlImage = "\(BaseURLSplash)\(dict_Login_Data.value(forKey: "ProfileImage")!)"
            cell.profileimage.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: "no-image"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                print(url ?? 0)
            }, usingActivityIndicatorStyle: .gray)
            cell.contentView.backgroundColor = hexStringToUIColor(hex: primaryColor)
            return cell
        }else{
            let cell = tvlist.dequeueReusableCell(withIdentifier: "Drawer2", for: indexPath as IndexPath) as! DrawerCell
           let dict = aryList.object(at: indexPath.row)as! NSDictionary
            cell.lblTitle.text = dict["title"]as? String
            cell.imageMenu.image = UIImage(named: "\(dict["image"]!)")
            return cell
        }
   
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 0){
            return 200

        }else{
            return UITableView.automaticDimension

        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section == 1){
        let dict = aryList.object(at: indexPath.row)as! NSDictionary
        UIView.animate(withDuration: 0.5) {
            self.tvlist.frame = CGRect(x: -self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            self.lblVersion.frame = CGRect(x: -self.view.frame.width, y: self.tvlist.frame.maxY, width: self.view.frame.width, height: 44)

            self.dismiss(animated: false) {
            self.handleDrawerView?.refreshDrawerScreen(strType: (dict["title"]as? String)!, tag: indexPath.row)
            }
        }
   }
 }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.alpha = 0.4
//        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
//        UIView.animate(withDuration: 1.0) {
//            cell.alpha = 1
//            cell.transform = .identity
//        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    
}

// MARK: - ----------------UserDashBoardCell
// MARK: -
class DrawerCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imageMenu: UIImageView!
    @IBOutlet weak var profileimage: UIImageView!
    @IBOutlet weak var lblName: UILabel!


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
