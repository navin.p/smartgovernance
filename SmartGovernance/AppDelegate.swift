//
//  AppDelegate.swift
//  SmartGovernance
//
//  Created by Navin Patidar on 3/4/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData
import Contacts
import AVFoundation
import Photos
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FTIndicator.setIndicatorStyle(UIBlurEffect.Style.dark)
        if DeviceType.IS_IPAD {
            mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        }else{
            mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        }
         allPermission()
         gotoViewController()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
      
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
     
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
   
        let container = NSPersistentContainer(name: "SmartGovernance")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
            
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
           
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    class func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    func gotoViewController()  {
        if (nsud.value(forKey: "SmartGovernance_LoginStatus") != nil) {
            if (nsud.value(forKey: "SmartGovernance_LoginStatus")as! String == "True") {
                let obj_ISVC : DynemicSplashVC = mainStoryboard.instantiateViewController(withIdentifier: "DynemicSplashVC") as! DynemicSplashVC
                let navigationController = UINavigationController(rootViewController: obj_ISVC)
                rootViewController(nav: navigationController)
                
            }else{
                let obj_ISVC : LoginVC = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                let navigationController = UINavigationController(rootViewController: obj_ISVC)
                rootViewController(nav: navigationController)
            }
        }else{
            let obj_ISVC : LoginVC = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let navigationController = UINavigationController(rootViewController: obj_ISVC)
            rootViewController(nav: navigationController)
        }
        
    }
    func rootViewController(nav : UINavigationController)  {
        self.window!.rootViewController = nav
        self.window!.makeKeyAndVisible()
        nav.setNavigationBarHidden(true, animated: true)
    }
    func allPermission()  {
        
        //Camera
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
            if response {
               
            } else {
                
            }
        }
        
        //Photos
        let photos = PHPhotoLibrary.authorizationStatus()
        if photos == .notDetermined {
            PHPhotoLibrary.requestAuthorization({status in
                if status == .authorized{
                } else {}
            })
        }
        // Contact
        guard #available(iOS 9.0, *) else { fatalError() }
        
        CNContactStore().requestAccess(for: .contacts) { _, _ in
            // callback(self.statusContacts)
        }
    }

}

